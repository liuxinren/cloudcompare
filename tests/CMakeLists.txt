project( unit-tests )

# Include CC header files
include_directories( ${CMAKE_SOURCE_DIR}/CC/include )

# Include GTest header files
include_directories( ${CMAKE_SOURCE_DIR}/libs/gtest/include )

# Include Qt
find_package( Qt 4 REQUIRED)
include_directories( ${QT_INCLUDE_DIR} )

# Include Boost

# Building the executables
file( GLOB test_source_list *.cpp )
add_executable(${PROJECT_NAME} ${test_source_list})
target_link_libraries(${PROJECT_NAME} gtest gtest_main ${QT_LIBRARIES} CC_CORE_LIB ${Boost_FILESYSTEM_LIBRARY} ${Boost_SYSTEM_LIBRARY})
add_test(${PROJECT_NAME} unit-tests)

add_dependencies(build_and_test unit-tests)

# Copy the resource folder
file( COPY ${CMAKE_CURRENT_SOURCE_DIR}/resource
        DESTINATION ${CMAKE_CURRENT_BINARY_DIR} )
