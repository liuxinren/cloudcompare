/* 
 * File:   ccImgConvexHullConfigDlg.h
 * Author: candrastefan
 *
 * Created on July 15, 2014, 11:55 AM
 */

#ifndef CCIMGCONVEXHULLCONFIG_H
#define	CCIMGCONVEXHULLCONFIG_H

// Local
#include <ccOverlayDialog.h>

// Qt
#include <QString>
#include <QStringList>

#include <ui_imgConvexHullConfigDlg.h>

class ccImgConvexHullConfig : public ccOverlayDialog, public Ui::imgConvexHullConfigDlg {
    Q_OBJECT

public:
    ccImgConvexHullConfig(QWidget* parent);
    virtual ~ccImgConvexHullConfig();

    QString getIntrinsicMatPath();
    QString getExtrinsicMatPath();
    QStringList getRefImagesPaths();
    size_t getN();    

protected slots:
    void loadIntrinsicMatPath();
    void loadExtrinsicMatPath();
    void loadRefImagesPaths();
    void updateIntrinsicMatPath(QString path);
    void updateExtrinsicMatPath(QString path);
    void updateRefImagesPath(QString path);
    void setN(const int);

protected:
    QString m_intrinsicMatPath;
    QString m_extrinsicMatPath;
    QStringList m_refImagesPaths;
    size_t m_N; // maximum number of reference images to show

private:

};

#endif	/* CCIMGCONVEXHULLCONFIG_H */

